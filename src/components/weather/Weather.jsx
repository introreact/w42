import React, { useState, useEffect } from "react";
import axios from "axios";

const Weather = () => {
    const [weather, setWeather] = useState([])
    const station = "Vaasa Klemettilä"

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios('https://www.voacap.com/geo/weather.html?city=Vaasa')
            setWeather(result.data)
            console.log(result.data)
        }
        fetchData()
    }, [])

    useEffect(() => {
        const interval = setInterval( async () => {
            const result = await axios('https://www.voacap.com/geo/weather.html?city=Vaasa')
            setWeather(result.data)
            console.log(result.data)
        }, 15000)
        return () => clearInterval(interval)

    }, [])
    const foundWeather = weather.find(w => w.station === station)
    if(foundWeather) {
        return(
            <div>
                <h1>Vaasa's Weather</h1>
                <p>{foundWeather.temperature}</p>
                <p>{foundWeather.pressure}</p>
            </div>
        )
    } else {
        return (
            <div>
                <h1>Vaasa's Weather</h1>
            </div>
        )
    }

}

export default Weather